package com.union.finanttial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinanttialApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinanttialApplication.class, args);
    }

}
